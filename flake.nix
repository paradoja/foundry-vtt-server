{
  description = "Running FoundryVTT as a service";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        devShell = let
          inherit (nixpkgs.lib) optional optionals;

          nodejs = pkgs.nodejs-18_x;
        in pkgs.mkShell {
          buildInputs = with pkgs; [
            # general & building
            git
            nodejs
            openssl

            # nix & bash (this file ;) )
            nixfmt
            rnix-lsp
            nodePackages.bash-language-server
            shellcheck
          ];
          shellHook = "";
        };
      });
}
